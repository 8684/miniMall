package com.miniMall.dao;

import com.miniMall.dao.BaseDao;
import com.miniMall.entity.SysOssEntity;

/**
 * 文件上传Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-03-25 12:13:26
 */
public interface SysOssDao extends BaseDao<SysOssEntity> {

}
