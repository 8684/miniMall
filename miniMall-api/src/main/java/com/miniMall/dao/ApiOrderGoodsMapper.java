package com.miniMall.dao;

import com.miniMall.dao.BaseDao;
import com.miniMall.entity.OrderGoodsVo;

/**
 * 
 * 
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-11 09:16:46
 */
public interface ApiOrderGoodsMapper extends BaseDao<OrderGoodsVo> {
	
}
