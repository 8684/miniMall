package com.miniMall.dao;

import com.miniMall.dao.BaseDao;
import com.miniMall.entity.TopicVo;

/**
 * 
 * 
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-11 09:16:46
 */
public interface ApiTopicMapper extends BaseDao<TopicVo> {
	
}
