/**   
 * Copyright © 2018 
 * 
 * 
 * @author: evi
 * @email : 4505957@qq.com
 * @date  : 2018年7月17日 下午3:49:39 
 */
package com.miniMall.util;

import com.xxl.emoji.EmojiTool;
import com.xxl.emoji.encode.EmojiEncode;

/**
 * @author evi
 *
 */
/**
 * 表情处理类
 * @author Administrator
 *
 */
public final class EmojiUtil {

    /**
     * 将emojiStr转为 带有表情的字符
     * @param emojiStr
     * @return
     */
    public static String emojiConverterUnicodeStr(String emojiStr){
         String result = EmojiTool.decodeToUnicode(emojiStr);
         
         return result;
    }
    
    /**
     * 带有表情的字符串转换为编码
     * @param str
     * @return
     */
    public static String emojiConverterToAlias(String str){
        String result=EmojiTool.encodeUnicode(str, EmojiEncode.ALIASES);
        return result;
    }
    public static void main(String[] args) {
    	System.out.println(EmojiTool.encodeUnicode(("✨🔥清新的流氓🔥✨"), EmojiEncode.ALIASES));
	}
}
