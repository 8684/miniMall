package com.miniMall.utils;

import java.util.Map;

public interface DealMapValueHelper {
	void dealValue(String key, Map<String, Object> map);
}
