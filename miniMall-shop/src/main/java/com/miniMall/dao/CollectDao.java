package com.miniMall.dao;

import com.miniMall.entity.CollectEntity;

/**
 * 
 * 
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-13 10:41:06
 */
public interface CollectDao extends BaseDao<CollectEntity> {
	
}
