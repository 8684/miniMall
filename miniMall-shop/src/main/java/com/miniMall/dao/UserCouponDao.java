package com.miniMall.dao;

import com.miniMall.entity.UserCouponEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-19 15:40:33
 */
public interface UserCouponDao extends BaseDao<UserCouponEntity> {

}
