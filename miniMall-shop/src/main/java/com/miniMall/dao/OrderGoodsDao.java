package com.miniMall.dao;

import com.miniMall.entity.OrderGoodsEntity;

/**
 * 
 * 
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-13 10:41:09
 */
public interface OrderGoodsDao extends BaseDao<OrderGoodsEntity> {
	
}
